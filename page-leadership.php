<?php

get_header();

	?><main id="content"> <!-- for ADA compliance -->
	
<?php
		if (has_post_thumbnail()):
			?><div class="banner preload" style="background-image: url('<?php echo get_the_post_thumbnail_url($post, 'full') ?>');">
				<div class="text<?php if (!empty (get_the_field('headline'))) echo ' has_headline'; ?>">
					<h1><?php the_title() ?></h1>
					<div class="headline"><?php echo save_orphans(get_the_field('headline')) ?></div>
				</div>
				<div class="corner"></div>
			</div>
			<div class="content_inner"><?php
		else:
			?><div class="content_inner">
				<h1><?php the_title() ?></h1><?php
		endif;

		if (have_posts()):
			while (have_posts()):
				the_post();
				the_content();
			endwhile;
		endif;
?>
		
	</main><?php
		
get_template_part('template-parts/funnels');

get_footer();

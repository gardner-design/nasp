<?php

get_header();

	?><main id="content"> <!-- for ADA compliance -->
	
		<div class="content_inner">
			<h1><?php the_title() ?></h1>
			<div class="info">
<?php
				if (get_the_date('Y') == date('Y')):
					echo get_the_date('M d'); else: echo get_the_date('M d, Y');
				endif;
				
				$term = get_the_terms( $post, 'category' )[0];
				if ($term->name != 'Uncategorized'):
					echo ' &middot; <a href="' . get_url('category/' . $term->slug) . '">' . $term->name . '</a>';
				endif;
?>
			</div>
<?php
			
			if (!empty(get_the_field('nasp_title'))):
				?><div class="nasp_title"><?php the_field('nasp_title') ?></div><?php
			endif;
			
			/* if (has_post_thumbnail()):
				echo '<div class="image_outer">';
				the_post_thumbnail();
				echo '</div>';
			endif; */
			
			if (have_posts()):
				while (have_posts()):
					the_post();
					the_content();
					
					?><div class="contact_info"><?php
						
					if (!empty(get_the_field('phone'))):
					
						?><p>
							Phone: <a href="tel:<?php the_field('phone') ?>"><?php the_field('phone') ?></a>
						</p><?php
					
					endif;
					if (!empty(get_the_field('fax'))):
					
						?><p>
							Fax: <?php the_field('fax') ?>
						</p><?php
					
					endif;
					if (!empty(get_the_field('email'))):
					
						?><p>
							<a href="mailto:<?php the_field('email') ?>"><?php the_field('email') ?></a>
						</p><?php
					
					endif;
					
					?></div><?php
					
				endwhile;
			endif;
?>
	    </div>
		<div class="next_article">
			<div class="next_article_inner">			
				<div class="heading">
					Read Next
				</div>
<?php
				$article = get_previous_post();
				if (empty($article)) {
					$articles = get_posts([
						'post_type' => 'post',
						'posts_per_page' => 1,
						'order' => 'DESC'
				    ]);
					foreach ($articles as $a) {
						$article = $a;
					}
				}
				setup_postdata($article);
?>
				<a class="article<?php if (has_post_thumbnail($article)) echo ' has_image' ?>" href="<?php echo get_permalink($article) ?>">
					
					<?php if (has_post_thumbnail($article)): ?><span class="image" <?php bg_image(get_the_post_thumbnail_url( $article, 'medium' )) ?>></span><?php endif; ?>
					<span class="title">
						<?php echo save_orphans(get_the_title($article)) ?>
					</span>
					<span class="date">
						<?php echo get_the_date('M d', $article) ?>
					</span>
					<span class="excerpt">
						<?php echo get_the_excerpt($article) ?> <span class="fake_link">read more</span>
					</span>
				</a>
<?php
				wp_reset_postdata();
?>
			</div>
		</div>
	</main><?php

get_footer();

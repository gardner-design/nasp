<?php

get_header();

	?><main id="content"> <!-- for ADA compliance -->
	
<?php
		if (has_post_thumbnail()):
			?><div class="banner preload" style="background-image: url('<?php echo get_the_post_thumbnail_url($post, 'full') ?>');">
				<div class="text<?php if (!empty (get_the_field('headline'))) echo ' has_headline'; ?>">
					<h1><?php the_title() ?></h1>
					<div class="headline"><?php echo save_orphans(get_the_field('headline')) ?></div>
				</div>
				<div class="corner"></div>
			</div>
			<div class="content_inner"><?php
		else:
			?><div class="content_inner">
				<h1><?php the_title() ?></h1><?php
		endif;

		if (have_posts()):
			while (have_posts()):
				the_post();
				the_content();
			endwhile;
		endif;
?>
		</div>
<?php
	
	if (!$post->post_parent):
	
?>
		<section class="bottom_action">
			<div class="bottom_action_inner">
					
				<h3>
					View member benefits and pricing.
				</h3>
				
				<p>
					Our members enjoy a wide variety of benefits. To see all the details and pricing, select the type of membership you’re interested in.
				</p>
				<p>
					NASP offers two distinct benefit packages for each corporate membership level. <strong>Package 1</strong> offers benefits that may be better suited for our specialty pharmacy members, while benefit <strong>Package 2</strong> is designed to meet the needs of our channel partners who support the pharmacies and pharmacists. Please select the option that best meets your needs. Membership dues are the same for either package. 
				</p>
				
			    <div class="membership_type_selector">
				    <form autocomplete="off">
				    	<fieldset>
					    	<label for="profession">Corporate</label>
					    	<select name="corp_type" id="corp_type">
						    	<option></option>
								<option value="1">
									Package 1
								</option>
								<option value="2">
									Package 2
								</option>
						    </select>
				    	</fieldset><fieldset>
					    	<label for="profession">Individual</label>
					    	<select name="indiv_type" id="indiv_type">
						    	<option></option><?php
	
								$args = [
									'post_type' => 'plan',
									'nopaging' => 1,
									'meta_key' => 'type',
									'meta_value' => 'individual'
								];
								$plans = get_posts($args);
								if (!empty($plans)):
									foreach ($plans as $plan):
									
									?><option value="<?php echo $plan->post_name ?>">
										<?php echo $plan->post_title ?>
									</option><?php
					
									endforeach;
								endif;
	
						    ?></select>
			    	</fieldset>
				    </form>
			    </div>
				
			</div>
		</section>
<?php
	
	endif;
	
?>
			
<?php /*
			<div class="wp-block-button is-style-squared">
				<a class="wp-block-button__link select_plan_button">
					Select Your Membership Plan
				</a>
			</div>
			
		    <div class="popup" id="membership_selection_popup">
			    <div class="popup_inner">
				    
					<form>
				
						<div class="pages_background"></div>
				    	
				    	<input type="radio" id="membership_type_indiv" name="membership_type" value="individual" />
						<input type="radio" id="membership_type_corp" name="membership_type" value="corporate" />
						
					    <input type="radio" id="is-certified-pharm-yes" name="is-certified-pharm" value="yes" />
						<input type="radio" id="is-certified-pharm-no" name="is-certified-pharm" value="no" />
						
					    <input type="radio" id="benefits-package-a" name="benefits-package" value="a" />
						<input type="radio" id="benefits-package-b" name="benefits-package" value="b" />
					    
					    <div class="pages paged_0">
					    	<div class="page page_1">
						    	<div class="page_inner">
							    	<h2>
								    	How many individuals?
							    	</h2>
							    	
							    	<label for="membership_type_indiv">
							    		1 <span>Individual Membership</span>
							    	</label><label for="membership_type_corp">
							    		2+ <span>Corporate Membership</span>
							    	</label>
						    	</div>
					    	</div><div class="page page_2">
						    	<div class="page_inner">
							    	<h2>
								    	What's your profession?
							    	</h2>
							    	
							    	<fieldset>
								    	<label for="profession">Profession</label>
								    	<select name="profession" id="profession">
									    	<option></option>
	<?php
										$args = [
											'post_type' => 'plan',
											'nopaging' => 1,
											'meta_key' => 'type',
											'meta_value' => 'individual'
										];
										$plans = get_posts($args);
										if (!empty($plans)):
											foreach ($plans as $plan):
											
											?><option value="<?php echo $plan->post_name ?>">
												<?php echo $plan->post_title ?>
											</option><?php
							
											endforeach;
										endif;
	?>
									    </select>
							    	</fieldset>
						    	</div>
					    	</div><div class="page page_3">
						    	<div class="page_inner">
							    	<h2>
								    	Exactly how many?
							    	</h2>
							    	
							    	<fieldset>
									    <label for="corporate-plan">Number of members</label>
								    	<select name="corporate-plan" id="corporate-plan">
									    	<option></option>
									    	<option value="copper">
									    		2
									    	</option>
									    	<option value="nickel">
									    		3-5
									    	</option>
									    	<option value="bronze">
									    		6-10
									    	</option>
									    	<option value="silver">
									    		11-20
									    	</option>
									    	<option value="gold">
									    		21-30
									    	</option>
									    	<option value="platinum">
									    		31-50
									    	</option>
									    	<option value="palladium">
									    		51-75
									    	</option>
								    	</select>
							    	</fieldset>
						    	</div>
					    	</div><div class="page page_5">
						    	<div class="page_inner">
							    	<h2>
								    	Choose your benefits package.
							    	</h2>
<?php
									$args = [
										'post_type' => 'plan',
										'nopaging' => 1,
										'meta_key' => 'type',
										'meta_value' => 'corporate'
									];
									$plans = get_posts($args);
									if (!empty($plans)):
										foreach ($plans as $plan):
										
											?><div class="package_benefits <?php echo $plan->post_name ?>">
												<div class="option option-a">
											    	<label for="benefits-package-a">
											    		Package A
											    	</label>
											    	<?php echo get_post_meta($plan->ID, 'package_a', true) ?>
										    	</div><div class="option option-b">
											    	<label for="benefits-package-b">
											    		Package B
											    	</label>
											    	<?php echo get_post_meta($plan->ID, 'package_b', true) ?>
										    	</div>
									    	</div><?php
													
										endforeach;
									endif;
?>
					    		</div>
						    </div><div class="page page_6">
						    	<div class="page_inner">
							    
							    	<!-- Benefits -->
<?php
									$args = [
										'post_type' => 'plan',
										'nopaging' => 1
									];
									$plans = get_posts($args);
									if (!empty($plans)):
										foreach ($plans as $plan):
																
											$plan_type = get_post_meta($plan->ID, 'type', true);
											$plan_price = get_post_meta($plan->ID, 'price', true);
											
											if ($plan_type == 'corporate'):
												
												$packages = (object) [
													'a' => (object) [
														'title' => $plan->post_title,
														'content' => $plan->post_content . '<h4>Package A</h4>' . get_post_meta($plan->ID, 'package_a', true),
														'url' => get_post_meta($plan->ID, 'package_a_url', true)
													],
													'b' => (object) [
														'title' => $plan->post_title,
														'content' => $plan->post_content . '<h4>Package B</h4>' . get_post_meta($plan->ID, 'package_b', true),
														'url' => get_post_meta($plan->ID, 'package_b_url', true)
													]
												];
												
											else:
											
												$packages = (object) [ (object) [
													'title' => $plan->post_title,
													'content' => $plan->post_content,
													'url' => get_post_meta($plan->ID, 'url', true)
												]];
												
											endif;
											
											foreach ($packages as $package_id => $package):
											
												// print_r($package);
												
												$url = get_post_meta($plan->ID, 'url', true);
													
												?><div class="benefits <?php echo $plan_type . '-' . $plan->post_name . ($package_id == 'a' || $package_id == 'b' ? '-' . $package_id : '') ?>">
											    	
											    	<h2>
												    	<?php echo $package->title ?> Membership
											    	</h2>
											    	
											    	<?php echo $package->content ?>
											    	
											    	<h3>
												    	$<?php echo $plan_price ?>/year
											    	</h3>
											    	
													<div class="wp-block-button is-style-squared">
														<a class="wp-block-button__link select_plan_button" href="<?php echo $package->url ?>">
															Register
														</a>
													</div>
													
										    	</div><?php
											    	
											endforeach;
						
										endforeach;
									endif;
?>
<!--
							    	<div class="benefits individual-other_benefits individual-pharmacist">
								    	
								    	<h2>
									    	Pharmacist or Other Health Care Professional
								    	</h2>
								    	
								    	<ul>
									    	<li>
										    	Access to the NASP membership portal, which includes educational resources and advocacy documents
									    	</li>
											<li>
												Newsworthy Notes – Receive the Daily Specialty Pharmacy SmartBrief, Quarterly NASP Membership Newsletter, and Biweekly Washington Regulatory and Legislative Updates
											</li>
											<li>
												Receive discounted registration rates for the NASP annual meeting & expo
											</li>
											<li>
												Preferred access to NASP educational/CE programs
											</li>
											<li>
												Participate in the “Ask the Expert Round Table Series” discussions
											</li>
								    	</ul>
								    	
								    	<h3>
									    	$250
								    	</h3>
								    	
										<div class="wp-block-button is-style-squared">
											<a class="wp-block-button__link select_plan_button">
												Register
											</a>
										</div>
										
							    	</div>
-->
						    	</div>
						    	
						    </div>
						    
					    	<div class="page_nav">
						    	<div class="page_nav_inner">
							    	<button type="button" class="cancel_button">
								    	Cancel
							    	</button>
							    	<button type="button" class="page_prev_button">
								    	Previous
							    	</button>
							    	<button type="button" class="page_next_button">
								    	Next
									</button>
									<div class="dots">
										<div class="dot"></div><div class="dot"></div><div class="dot"></div><div class="dot"></div>
									</div>
						    	</div>
					    	</div>
				    	</div>
				    	
					</form>
					
		    	</div>
		    </div> <!-- End .membership_selection_popup --> */ ?>
			    
	    
	</main><?php

get_template_part('template-parts/funnels');

get_footer();

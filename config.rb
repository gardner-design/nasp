preferred_syntax = :sass

project_path = '/Users/shane/Desktop/local sites/naspnet/app/public/wp-content/themes/nasp/'
http_path = '/naspnet/'
css_dir = 'assets/css'
sass_dir = 'assets/sass'
fonts_dir = 'assets/fonts'
images_dir = 'assets/images'
javascripts_dir = 'assets/js'

relative_assets = true
line_comments = false

output_style = :compressed
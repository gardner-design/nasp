<?php
	
$funnels = get_the_field('funnels');
if (!empty($funnels)):

?>
	<div class="funnels">
<?php

	$pages = explode('|', $funnels);

	foreach ($pages as $post_id):
		$post = get_post($post_id);
		
		$url = get_permalink($post);
		if (!empty(get_the_field('bucket_url'))) {
			$url = get_the_field('bucket_url');
		}
		
		$text = get_the_title($post);
		if (!empty(get_the_field('bucket_text'))) {
			$text = get_the_field('bucket_text');
		}
		
		$image_url = get_the_post_thumbnail_url($post, 'large');

		?><a href="<?php echo $url ?>">
			<span class="image" style="background-image: url('<?php echo $image_url ?>');"></span>
			<span class="text">
				<?php echo $text ?>
			</span>
		</a><?php

	endforeach;
	wp_reset_postdata();
	
?>
	</div>
<?php

endif;
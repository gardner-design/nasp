<?php

get_header();

	?><main id="content"> <!-- for ADA compliance -->
	
		<div class="content_inner">
<?php
			if (have_posts()):
				while (have_posts()):
					the_post();
?>
				    <div class="breadcrumb">
					    <?php echo ucfirst(get_the_field('type')) ?> Membership
				    </div>
					<h1>
				    	<?php the_title() ?>
			    	</h1>
			    	<h2>
				    	$<?php the_field('price') ?>/year
			    	</h2>
<?php
					the_content();
					
					if (get_the_field('type') == 'corporate'):
						
						$packages = (object) [
							'a' => (object) [
								'title' => 'Package A',
								'content' => get_the_field('package_a'),
								'url' => get_the_field('package_a_url')
							],
							'b' => (object) [
								'title' => 'Package B',
								'content' => get_the_field('package_b'),
								'url' => get_the_field('package_b_url')
							]
						];
						foreach ($packages as $package_id => $package):
							
							?><h3>
						    	<?php echo $package->title ?>
					    	</h3>
					    	
					    	<?php echo $package->content ?>
					    	
							<div class="wp-block-button is-style-squared">
								<a class="wp-block-button__link select_plan_button" href="<?php echo $package->url ?>">
									Choose <?php the_title() ?> <?php echo $package->title ?>
								</a>
							</div><?php
						    	
						endforeach;
						
					else:
?>
						<div class="wp-block-button is-style-squared">
							<a class="wp-block-button__link select_plan_button" href="<?php the_field('url') ?>">
								Join
							</a>
						</div>
<?php
					endif;
					
					
				endwhile;
			endif;
?>
	    </div>
	</main><?php

get_footer();

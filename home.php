<?php

get_header();

	?><main id="content"> <!-- for ADA compliance -->
        <?php
/*
		if (!empty(single_cat_title('', false))):
			
			?><a class="all_news" <?php url('news') ?>>News</a>
			<h1><?php single_cat_title() ?></h1><?php
				
		else:
			?><h1>News</h1><?php
		endif;
*/
		?>
		
		<div class="news_categories">
			<h1>News</h1>
			<?php /* <h2>Categories</h2> */ ?>
			<div class="categories_outer">
				<a <?php url('news') ?>>
					All
				</a><?php
	
				foreach (get_categories(array( 'number' => 8, 'parent' => 0 )) as $cat):
					if (empty($cat->parent)):
		
					?><a <?php url(get_category_link($cat)) ?>>
						<?php echo $cat->name ?></a><?php
		
					endif;
				endforeach;
					
				?><a <?php url('community-news') ?>>
					Current Community News
				</a>
			</div>
		</div>
		<div class="content_inner">
		    <div class="articles">
				<ul>
<?php
				if (have_posts()): while (have_posts()): the_post();
		
					?><li>
						<a class="post_<?php the_ID(); if (has_post_thumbnail()) echo ' has_image'; ?>" href="<?php echo get_permalink() ?>">
							
							<?php if (has_post_thumbnail()): ?><span class="image" <?php bg_image(get_the_post_thumbnail_url( $post, 'medium' )) ?>></span><?php endif; ?>
							<span class="title">
								<?php the_title() ?>
							</span>
							<span class="date">
								<?php if (get_the_date('Y') == date('Y')): echo get_the_date('M d'); else: echo get_the_date('M d, Y'); endif; ?>
							</span>
							<span class="excerpt">
								<?php echo get_the_excerpt() ?> <span class="fake_link">read more</span>
							</span>
						</a>
					</li><?php
		
				endwhile; endif;
?>
				</ul>
			</div>
	    
	    </div>
	    <div class="pagination">
<?php

			// Previous/next page navigation
			$args = array(
				'show_all'           => false,
				'prev_next'          => true,
				'prev_text'          => __('Newer'),
				'next_text'          => __('Older'),
				'type'               => 'array'
			);
			if (!empty(paginate_links($args))) {
				foreach (paginate_links($args) as $html) {
					echo $html;
				}
			}

?>
		</div>
	</main><?php

get_footer();

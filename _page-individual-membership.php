<?php

get_header();

	?><main id="content"> <!-- for ADA compliance -->
	
<?php
		if (has_post_thumbnail()):
			?><div class="banner preload" style="background-image: url('<?php echo get_the_post_thumbnail_url($post, 'full') ?>');">
				<div class="text<?php if (!empty (get_the_field('headline'))) echo ' has_headline'; ?>">
					<h1><?php the_title() ?></h1>
					<div class="headline"><?php echo save_orphans(get_the_field('headline')) ?></div>
				</div>
				<div class="corner"></div>
			</div>
			<div class="content_inner"><?php
		else:
			?><div class="content_inner">
				<h1><?php the_title() ?></h1><?php
		endif;

		if (have_posts()):
			while (have_posts()):
				the_post();
				the_content();
			endwhile;
		endif;
 ?>
		</div>
		<section class="bottom_action">
			<div class="bottom_action_inner">
				<form id="profession_form">
					
					<h3>
						Register today.
					</h3>
					<p>
						Choose your individual type to see pricing and to register.
					</p>

			    	<fieldset>
				    	<label for="profession">Individual Membership Type</label>
				    	<select name="profession" id="profession">
					    	<option></option>
<?php
						$args = [
							'post_type' => 'plan',
							'nopaging' => 1,
							'meta_key' => 'type',
							'meta_value' => 'individual'
						];
						$plans = get_posts($args);
						if (!empty($plans)):
							foreach ($plans as $plan):
							
							?><option value="<?php echo $plan->post_name ?>">
								<?php echo $plan->post_title ?>
							</option><?php
			
							endforeach;
						endif;
?>
					    </select>
			    	</fieldset>
			    	
					<p>
						Are you looking for a <a <?php url('corporate-membership') ?>>corporate membership package</a>?
					</p>
					
				</form>
			
				<!-- Benefits -->
<?php
				$args = [
					'post_type' => 'plan',
					'nopaging' => 1,
					'meta_key' => 'type',
					'meta_value' => 'individual'
				];
				$plans = get_posts($args);
				if (!empty($plans)):
					foreach ($plans as $plan):
											
						$plan_price = get_post_meta($plan->ID, 'price', true);
						
						$package = (object) [
							'title' => $plan->post_title,
							'content' => $plan->post_content,
							'url' => get_post_meta($plan->ID, 'url', true)
						];
					
						$url = get_post_meta($plan->ID, 'url', true);
							
						?><section class="benefits <?php echo $plan->post_name ?>">
					    	
					    	<a class="back_to_profession">Select another membership type</a>
					    	<h2>
						    	<?php echo $package->title ?> Membership
					    	</h2>
					    	
					    	<?php echo $package->content ?>
					    	
					    	<h3>
						    	$<?php echo $plan_price ?>/year
					    	</h3>
					    	
							<div class="wp-block-button is-style-squared">
								<a class="wp-block-button__link select_plan_button" href="<?php echo $package->url ?>">
									Register
								</a>
							</div>
						</section><?php
						    	
					endforeach;
				endif;
?>
			</div>
		</section>
	</main><?php

get_template_part('template-parts/funnels');

get_footer();

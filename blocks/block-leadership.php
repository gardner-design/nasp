<h2><?php echo (block_value('team') == 'Staff' ? 'NASP ' : '') . block_value('team') ?></h2>
<div class="board_members<?php if (block_value('team') == 'Consultant Partners') echo ' consultant_partners'; ?>">
	<ul><?php
	
		$args = array(
			'post_type' => 'member',
			'posts_per_page' => -1,
			'tax_query' => array(
				array(
					'taxonomy' => 'team',
					'field' => 'name',
					'terms' => block_value('team')
				)
			)
		);
		$board_members = get_posts( $args );
		foreach ($board_members as $board_member):
			
			$image_url = get_the_post_thumbnail_url($board_member, 'large');
			$name = $board_member->post_title;
			preg_match('/^(.*PharmD, )(.*)$/m', $board_member->post_title, $matches);
			if (count($matches) > 2) {
				$name = $matches[1] . '<span>' . $matches[2] . '</span>';
			}

			?><li>
				<a <?php url('/leadership/' . $board_member->post_name) ?>>
					<?php if (!empty($image_url)): ?><span class="image" <?php bg_image($image_url) ?>></span><?php endif; ?>
					<span class="name"><?php echo $name ?></span>
					<?php if (!empty($board_member->nasp_title)): ?><span class="nasp_title"><?php echo save_orphans($board_member->nasp_title) ?></span><?php endif; ?>
					<span class="fake_link">Learn more</span>
				</a>
			</li><?php
			
		endforeach;
		
	?></ul>
</div>
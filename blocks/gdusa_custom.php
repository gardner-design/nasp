<?php

// Init custom blocks for Gutenberg
function init_custom_blocks() {
	
	if (is_admin()) { // only load blocks if in admin
		wp_enqueue_editor();
	}

    wp_register_script(
        'custom_editor',
        get_stylesheet_directory_uri() . '/blocks/gdusa_custom.js',
        array( 'wp-blocks', 'wp-element', 'wp-editor' )
    );

    wp_register_style(
        'custom_editor',
        get_stylesheet_directory_uri() . '/assets/css/blocks/gdusa_custom.css',
        array( 'wp-edit-blocks' ),
        // filemtime( get_stylesheet_directory() . '/assets/css/blocks/gdusa_custom.css' )
    );

    register_block_type( 'gdusa/custom', array(
        'editor_script' => 'custom_editor',
        'editor_style'  => 'custom_editor',
		'render_callback' => 'custom_editor_render'
    ));
    
    register_block_type( 'gdusa/container', array(
        'editor_script' => 'custom_editor',
        'editor_style'  => 'custom_editor',
		'render_callback' => 'custom_container_render'
    ));

}
add_action( 'init', 'init_custom_blocks' );

function custom_editor_render( $attributes, $content ) {
	$content = html_entity_decode($content);
	return $content;
}
function custom_container_render( $attributes, $content ) {
	
	$content = preg_replace('/\n/','', $content); // remove new lines
	
	$content = preg_replace('/(<div class="wp-block-gdusa-custom">.*<div class="content">.*<\/div><\/div><\/div>)/', '<div class="wp-block-gdusa-custom-outer">$1</div>', $content);
	
	$content = preg_replace('/(<div class=\"wp-block-gdusa-container\">)(.*<\/div><\/div>)/', '$1<div class="wp-block-gdusa-container_inner">$2</div>', $content);
	
	$content = '</div>' . $content . '<div class="content_inner">';
	return $content;
}
	
var el = wp.element.createElement,
    Fragment = wp.element.Fragment,
    registerBlockType = wp.blocks.registerBlockType,
    MediaUpload = wp.editor.MediaUpload,
    RichText = wp.editor.RichText,
    BlockControls = wp.editor.BlockControls,
    InnerBlocks = wp.editor.InnerBlocks;

registerBlockType( 'gdusa/custom', {
    title: 'Half Block',
    icon: 'editor-code',
    category: 'layout',
    
    attributes: {
	    imageUrl: {
	        type: 'string',
	        source: 'attribute',
	        selector: 'img',
	        attribute: 'src',
	    },
        heading: {
            type: 'string',
            source: 'html',
            selector: 'h3',
        },
        content: {
            type: 'array',
            source: 'children',
            selector: 'div.content'
        },
    },
	
    edit: function({ attributes, setAttributes, className, isSelected }) {
        var imageUrl = attributes.imageUrl;
        var heading = attributes.heading;
        var content = attributes.content;
		
        function onSelectImage( media ) {
            setAttributes( { imageUrl: media.url } );
        }
        function onChangeHeading( data ) {
            setAttributes( { heading: data } );
        }
        function onChangeContent( data ) {
            setAttributes( { content: data } );
        }
		
        return (
            el(
                'div',
                {
                    className: className,
                },
                el(
                    BlockControls,
                    null
                ),
                el(
	                MediaUpload,
	                {
		                type: 'image',
                        onSelect: onSelectImage,
                        value: imageUrl,
                        render: function( obj ) {
							return el( wp.components.Button, {
								className: attributes.imageUrl ? 'image-button' : 'button button-large',
								onClick: obj.open
							},
							!attributes.imageUrl ? wp.i18n.__( 'Upload Image (optional)' ) : el( 'img', { src: attributes.imageUrl } )
						); }
	                }
                ),
                el(
                    RichText,
                    {
                        tagName: 'h3',
                        onChange: onChangeHeading,
                        value: heading,
                        placeholder: 'Heading',
                        keepPlaceholderOnFocus: true
                    }
                ),
                el(
                    RichText,
                    {
                        tagName: 'div',
                        className: 'content',
                        multiline: 'p',
                        onChange: onChangeContent,
                        value: content,
                        placeholder: 'Paragraph',
                        keepPlaceholderOnFocus: true
                    }
                )
            )
        );
    },

    save: function( props ) {
	    var imageUrl = props.attributes.imageUrl;
        var heading = props.attributes.heading;
        var content = props.attributes.content;
		
		return (
			el( 'div',
				{
					className: props.className
				},
				imageUrl && el( 'img', { src: imageUrl } ),
				heading && el( 'h3', {}, heading ),
				content && el( 'div', { className: 'content' }, content )
			)
		);
    },
} );

/*
registerBlockType( 'gdusa/text-grid', {
    title: 'Text Grid',
    icon: 'editor-code',
    category: 'layout',
    
    attributes: {
        heading: {
            type: 'string',
            source: 'html',
            selector: 'h2',
        },
        content: {
            type: 'array',
            source: 'children',
            selector: 'div.content'
        },
    },
	
    edit: function({ attributes, setAttributes, className, isSelected }) {
        var heading = attributes.heading;
        var content = attributes.content;
		
        function onChangeHeading( data ) {
            setAttributes( { heading: data } );
        }
        function onChangeContent( data ) {
            setAttributes( { content: data } );
        }
		
        return (
            el(
                'div',
                {
                    className: className,
                },
                el(
                    BlockControls,
                    null
                ),
                el(
                    RichText,
                    {
                        tagName: 'h2',
                        onChange: onChangeHeading,
                        value: heading,
                        placeholder: 'Heading',
                        keepPlaceholderOnFocus: true
                    }
                ),
                el(
                    RichText,
                    {
                        tagName: 'div',
                        className: 'content',
                        multiline: 'p',
                        onChange: onChangeContent,
                        value: content,
                        placeholder: 'Paragraph',
                        keepPlaceholderOnFocus: true
                    }
                )
            )
        );
    },

    save: function( props ) {
        var heading = props.attributes.heading;
        var content = props.attributes.content;
		
		return (
			el( 'div',
				{
					className: props.className
				},
				heading && el( 'h2', {}, heading ),
				content && el( 'div', { className: 'content' }, content )
			)
		);
    },
} );
*/


registerBlockType( 'gdusa/container', {
    title: 'Container',
    icon: 'editor-code',
    category: 'layout',
    
    attributes: {
        content: {
            type: 'array',
            source: 'children',
            selector: 'div'
        },
    },
	
    edit: function({ className }) {
        
        return (
            el(
                'div',
                {
                    className: className
                },
                el(
                    InnerBlocks,
                    null
                )
            )
        );
    },

    save: function( { className } ) {
		return (
			el(
				'div',
				{
                    className: className
				},
				el(
					'div',
					{},
					el(
						InnerBlocks.Content,
						null
					)
				)
			)
		);
    },
} );
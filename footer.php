	</div> <!-- #content -->
	
	<footer>
		
		<div class="phone_outer">
			<a href="tel:<?php echo get_global('phone') ?>" target="_blank"><?php echo get_global('phone') ?></a>
		</div>
<?php
	
		$is_certification = ($post->post_name == 'certification' || (!empty($post->post_parent) && get_post($post->post_parent)->post_name == 'certification'));
		
		
		$menu_slug = 'secondary';
		if ($is_certification) {
			$menu_slug = 'spcb';
		}
/*
		echo '<!-- spcb ';
		print_r(wp_get_nav_menu_object($menu_slug));
		echo ' -->';
		
*/
		if (has_nav_menu($menu_slug)):
		
			?><nav aria-label="Footer Nav">
<?php
			foreach (gdusa_get_nav_menu_items_tree($menu_slug) as $nav_item):
	
				?><a <?php url( $nav_item->url, true ) ?>>
					<?php echo $nav_item->title ?>
				</a><?php
	
			endforeach;
?>
			</nav><?php
			
		endif;
		
		if ($is_certification):
		
			?><div class="spcb_logos">
				<div class="ncca_logo"></div><div class="ice_logo"></div>
			</div><?php
				
		endif;
		if (!$is_certification):
		
			?><div class="social_media"><?php
				
				$social = array(
					'facebook' => 'Facebook',
					'twitter' => 'Twitter',
					'instagram' => 'Instagram',
					'youtube' => 'YouTube',
					'linked_in' => 'LinkedIn'
				);
				foreach($social as $social_id => $social_name):
					$social_url = get_option('globals-' . $social_id);
					if (empty($social_url))
						continue;
					
					?><a class="<?php echo $social_id ?>" href="<?php echo $social_url ?>" target="_blank">
						NASP on <?php echo $social_name ?>
					</a><?php
					
				endforeach;
			
			?></div><?php
				
		endif;

?>
		<div class="contact_info">
<?php
		if (get_the_ID() != '3715'):
		
			?><p>
				<?php echo ($is_certification ? 'Specialty Pharmacy Certification Board' : 'National Association of Specialty Pharmacy') ?><br>
				<?php echo get_global('address_street') ?><br>
				<?php echo get_global('address_city') ?>, <?php echo get_global('address_state') ?> <?php echo get_global('address_zip') ?>
			</p><?php
				
		else:
		
			?><p>
				NASP PAC<br>
				PO BOX 1911<br>
				Alexandria, VA 22313-1911
			</p><?php
		
		endif;
?>
		</div>
		
		<div class="legal_outer"><?php
			
		if ($is_certification):
		
			?><a href="https://naspnet.org/wp-content/uploads/2021/02/SPCB-Charter_2016-06-22_Final.pdf" target="_blank">
				SPCB Governance Charter
			</a><?php
				
		else:
		
			?><a href="<?php echo site_url() ?>/bylaws" target="_blank">
				Bylaws
			</a><?php
				
		endif;
		
			?><a href="<?php echo site_url() ?>/privacy-policy">
				Privacy
			</a>
		</div>
		
	</footer>

<?php wp_footer() ?>

</body>
</html>
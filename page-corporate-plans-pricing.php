<?php

get_header();

	?><main id="content"> <!-- for ADA compliance -->
	
<?php
		if (has_post_thumbnail()):
			?><div class="banner preload" style="background-image: url('<?php echo get_the_post_thumbnail_url($post, 'full') ?>');">
				<div class="text has_headline">
					<h1>
					Corporate Membership<?php if (isset($_GET['package']) && in_array($_GET['package'], array('1','2'))): echo ' (Package ' . $_GET['package'] . ')'; else: echo 's'; endif; ?>
					</h1>
					<div class="headline"><?php echo save_orphans(get_the_title()) ?></div>
				</div>
				<div class="corner"></div>
			</div>
			<div class="content_inner"><?php
		else:
			?><div class="content_inner">
				<div class="breadcrumb">
					Corporate Membership<?php if (isset($_GET['package']) && in_array($_GET['package'], array('1','2'))): echo ' (Package ' . $_GET['package'] . ')'; else: echo 's'; endif; ?>
				</div>
				<h1><?php the_title() ?></h1><?php
		endif;

		if (have_posts()):
			while (have_posts()):
				the_post();
				the_content();
			endwhile;
		endif;
 ?>
 
<?php if (!isset($_GET['package']) || !in_array($_GET['package'], array('1','2'))): ?>
		
			<p>
				<?php /* if (!isset($_GET['specialty-pharmacy']) && !isset($_GET['other'])): ?>
				
					Whether you’re a specialty pharmacy or not, there’s a membership level just for you. <a <?php url('membership/corporate-plans-pricing?specialty-pharmacy') ?>>Package A</a> is tailored for specialty pharmacies, and <a <?php url('membership/corporate-plans-pricing?other') ?>>Package B</a> is for other organizations.
				
				<?php else: ?>
				
					These corporate membership plans are tailored to best benefit your <?php echo (isset($_GET['specialty-pharmacy']) ? 'specialty pharmacy' : 'organization') ?>.
				
				<?php endif; */ ?>
				
				Whether you’re a specialty pharmacy or not, there’s a membership level just for you. View member benefits and pricing for <a href="<?php echo site_url('membership/corporate-plans-pricing?package=1') ?>">Package 1</a> and <a href="<?php echo site_url('membership/corporate-plans-pricing?package=2') ?>">Package 2</a>.
			
			</p>
		</div>
			
<?php else: ?>
				
			<p>
				Whether you’re a specialty pharmacy or not, there’s a membership level just for you. Compare and choose the level that fits your needs. The bullets indicate eligibility, with stars indicating preferential opportunities.
			</p>
			<p>
				<em>Key: Bullet = Eligible; Star = Preferential</em>
			</p>
			<p>
				Corporate Membership dues are prorated for members joining NASP in any month of the year other than January. This one-time discount is applied to the second year’s membership dues.
			</p>
		</div>
		
 		<div class="plans_table_outer">
			<div class="plans_table">
	 			<div class="plans_table_inner">
		 			<div class="row table_head">
			 			<div>
				 			
			 			</div>
			 			<div>
				 			<span>Palladium</span>
			 			</div>
			 			<div>
				 			<span>Platinum</span>
			 			</div>
			 			<div>
				 			<span>Gold</span>
			 			</div>
			 			<div>
				 			<span>Silver</span>
			 			</div>
			 			<div>
				 			<span>Bronze</span>
			 			</div>
			 			<div>
				 			<span>Nickel</span>
			 			</div>
			 			<div>
				 			<span>Copper</span>
			 			</div>
		 			</div>
		 			<div class="row count_row">
			 			<div>
				 			Memberships
			 			</div>
			 			<div>
				 			75
			 			</div>
			 			<div>
				 			50
			 			</div>
			 			<div>
				 			30
			 			</div>
			 			<div>
				 			20
			 			</div>
			 			<div>
				 			10
			 			</div>
			 			<div>
				 			5
			 			</div>
			 			<div>
				 			2
			 			</div>
		 			</div>
		 			<div class="row section_heading_row">
			 			<div>
				 			Annual Meeting & Expo
			 			</div>
			 			<div>
				 			
			 			</div>
			 			<div>
				 			
			 			</div>
			 			<div>
				 			
			 			</div>
			 			<div>
				 			
			 			</div>
			 			<div>
				 			
			 			</div>
			 			<div>
				 			
			 			</div>
			 			<div>
				 			
			 			</div>
		 			</div>
		 			<div class="row section_child_row">
			 			<div>
				 			Members-only discounted registration fees for those listed on your corporate roster
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
		 			</div>
		 			
		 			<?php if ($_GET['package'] == '2'): ?>
		 			
			 			<div class="row section_child_row">
				 			<div>
					 			Complimentary Annual Meeting & Expo <strong>FULL registrations</strong> (includes workshops)
				 			</div>
				 			<div class="">
					 			3
				 			</div>
				 			<div class="">
					 			2
				 			</div>
				 			<div class="">
					 			2
				 			</div>
				 			<div class="">
					 			2
				 			</div>
				 			<div class="">
					 			1
				 			</div>
				 			<div>
					 			1
				 			</div>
				 			<div>
					 			
				 			</div>
			 			</div>
			 			<div class="row section_child_row">
				 			<div>
					 			Complimentary <strong>Annual Meeting & Expo registrations</strong> (conference only, no workshops)
				 			</div>
				 			<div class="">
					 			8
				 			</div>
				 			<div class="">
					 			4
				 			</div>
				 			<div class="">
					 			3
				 			</div>
				 			<div class="">
					 			1
				 			</div>
				 			<div class="">
					 			1
				 			</div>
				 			<div>
					 			1
				 			</div>
				 			<div>
					 			
				 			</div>
			 			</div>
			 			<div class="row section_child_row">
				 			<div>
					 			Complimentary Annual Meeting & Expo Conference Workshop registrations
				 			</div>
				 			<div class="">
					 			
				 			</div>
				 			<div class="">
					 			1
				 			</div>
				 			<div class="">
					 			1
				 			</div>
				 			<div class="">
					 			1
				 			</div>
				 			<div class="">
					 			1
				 			</div>
				 			<div>
					 			
				 			</div>
				 			<div>
					 			1
				 			</div>
			 			</div>
			 			<div class="row section_child_row">
				 			<div>
					 			Discount on Annual Meeting & Expo exhibits
				 			</div>
				 			<div>
					 			17.5%
				 			</div>
				 			<div>
					 			15%
				 			</div>
				 			<div>
					 			10%
				 			</div>
				 			<div>
					 			5%
				 			</div>
				 			<div>
					 			3%
				 			</div>
				 			<div>
					 			2%
				 			</div>
				 			<div>
					 			2%
				 			</div>
			 			</div>
			 			<div class="row section_child_row">
				 			<div>
					 			Discount on Annual Meeting & Expo sponsorships
				 			</div>
				 			<div>
					 			15%
				 			</div>
				 			<div>
					 			15%
				 			</div>
				 			<div>
					 			10%
				 			</div>
				 			<div>
					 			7%
				 			</div>
				 			<div>
					 			3%
				 			</div>
				 			<div>
					 			3%
				 			</div>
				 			<div>
					 			3%
				 			</div>
			 			</div>
		 			
		 			<?php endif; ?>
		 			
		 			<?php if ($_GET['package'] == '1'): ?>
		 			
			 			<div class="row section_child_row">
				 			<div>
					 			Complimentary <strong>Annual Meeting & Expo registrations</strong> (conference only, no workshops)
				 			</div>
				 			<div class="">
					 			8
				 			</div>
				 			<div class="">
					 			4
				 			</div>
				 			<div class="">
					 			3
				 			</div>
				 			<div class="">
					 			2
				 			</div>
				 			<div class="">
					 			1
				 			</div>
				 			<div>
					 			1
				 			</div>
				 			<div>
					 			
				 			</div>
			 			</div>
			 			<div class="row section_child_row">
				 			<div>
					 			Complimentary conference workshop registrations
				 			</div>
				 			<div class="">
					 			4
				 			</div>
				 			<div class="">
					 			2
				 			</div>
				 			<div class="">
					 			2
				 			</div>
				 			<div class="">
					 			1
				 			</div>
				 			<div class="">
					 			1
				 			</div>
				 			<div>
					 			
				 			</div>
				 			<div>
					 			1
				 			</div>
			 			</div>
			 			<div class="row section_child_row">
				 			<div>
					 			Complimentary <strong>Live CSP Exam Prep Course</strong> registrations at NASP Annual Meeting & Expo
				 			</div>
				 			<div class="">
					 			2
				 			</div>
				 			<div class="">
					 			1
				 			</div>
				 			<div class="">
					 			1
				 			</div>
				 			<div class="">
					 			1
				 			</div>
				 			<div class="">
					 			
				 			</div>
				 			<div>
					 			
				 			</div>
				 			<div>
					 			
				 			</div>
			 			</div>
			 			<div class="row section_child_row">
				 			<div>
					 			Discount on exhibits and sponsorships
				 			</div>
				 			<div class="">
					 			15%
				 			</div>
				 			<div class="">
					 			15%
				 			</div>
				 			<div class="">
					 			10%
				 			</div>
				 			<div class="">
					 			5%
				 			</div>
				 			<div class="">
					 			
				 			</div>
				 			<div>
					 			
				 			</div>
				 			<div>
					 			
				 			</div>
			 			</div>
		 			
		 			<?php endif; ?>
		 			
		 			<div class="row section_child_row">
			 			<div>
			 				Annual Meeting & Expo speaking opportunities
			 			</div>
			 			<div class="preferential">
				 			
			 			</div>
			 			<div class="preferential">
				 			
			 			</div>
			 			<div class="preferential">
				 			
			 			</div>
			 			<div class="preferential">
				 			
			 			</div>
			 			<div class="preferential">
				 			
			 			</div>
			 			<div class="preferential">
				 			
			 			</div>
			 			<div class="preferential">
				 			
			 			</div>
		 			</div>
		 			
		 			<?php if ($_GET['package'] == '1'): ?>
		 			
			 			<div class="row">
				 			<div>
				 				Complimentary online CSP Exam Prep courses
				 			</div>
				 			<div>
					 			2
				 			</div>
				 			<div>
					 			2
				 			</div>
				 			<div>
					 			2
				 			</div>
				 			<div>
					 			2
				 			</div>
				 			<div>
					 			2
				 			</div>
				 			<div>
					 			1
				 			</div>
				 			<div>
					 			1
				 			</div>
			 			</div>
		 			
		 			<?php endif; ?>
		 			
		 			<div class="row">
			 			<div>
			 				State level legislation monitoring and tracking
			 			</div>
			 			<div>
				 			$0
			 			</div>
			 			<div>
				 			$0
			 			</div>
			 			<div>
				 			$0
			 			</div>
			 			<div>
				 			$0
			 			</div>
			 			<div>
				 			$5,000 per year**
			 			</div>
			 			<div>
				 			$5,000 per year**
			 			</div>
			 			<div>
				 			$5,000 per year**
			 			</div>
		 			</div>
		 			<div class="row">
			 			<div>
			 				Complimentary Formulary Academy licenses
			 			</div>
			 			<div>
				 			10
			 			</div>
			 			<div>
				 			5
			 			</div>
			 			<div>
				 			5
			 			</div>
			 			<div>
				 			3
			 			</div>
			 			<div>
				 			3
			 			</div>
			 			<div>
				 			3
			 			</div>
			 			<div>
				 			1
			 			</div>
		 			</div>
		 			<div class="row">
			 			<div>
			 				Corporate member recognition
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
		 			</div>
		 			<div class="row">
			 			<div>
			 				NASP Committee participation
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
		 			</div>
		 			
		 			<?php if ($_GET['package'] == '1'): ?>
		 			
			 			<div class="row">
				 			<div>
				 				Eligible for NASP Awards
				 			</div>
				 			<div class="eligible">
					 			
				 			</div>
				 			<div class="eligible">
					 			
				 			</div>
				 			<div class="eligible">
					 			
				 			</div>
				 			<div class="eligible">
					 			
				 			</div>
				 			<div class="eligible">
					 			
				 			</div>
				 			<div class="eligible">
					 			
				 			</div>
				 			<div class="eligible">
					 			
				 			</div>
			 			</div>
		 			
		 			<?php endif; ?>
		 			
		 			<div class="row">
			 			<div>
			 				Featured on NASP Podcast and Corporate Member Spotlight Program
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
		 			</div>
		 			<div class="row">
			 			<div>
			 				Featured in one “Lunch & Learn” webinar*
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div>
				 			
			 			</div>
		 			</div>
		 			<div class="row">
			 			<div>
			 				Join “Ask the Expert Roundtable” webinars
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
		 			</div>
		 			
		 			<?php if ($_GET['package'] == '1'): ?>
		 			
			 			<div class="row">
				 			<div>
				 				NASP Patient Satisfaction Survey Program: FREE standard survey ($1,695 value) OR Enhanced Survey at NASP member pricing (includes a $1,695 credit)
				 			</div>
				 			<div class="eligible">
					 			
				 			</div>
				 			<div class="eligible">
					 			
				 			</div>
				 			<div class="eligible">
					 			
				 			</div>
				 			<div class="eligible">
					 			
				 			</div>
				 			<div class="eligible">
					 			
				 			</div>
				 			<div class="eligible">
					 			
				 			</div>
				 			<div>
					 			Eligible to participate at NASP Member pricing
				 			</div>
			 			</div>
		 			
		 			<?php endif; ?>
		 			
		 			<div class="row">
			 			<div>
			 				NASP educational / CE programs
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
		 			</div>
		 			<div class="row">
			 			<div>
			 				Grassroots advocacy tool access
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
		 			</div>
		 			<div class="row">
			 			<div>
			 				SmartBrief (3 times per week), <em>The Advocate</em> quarterly NASP newsletter, bi-weekly <em>Washington Update</em> delivered directly to your email inbox
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
		 			</div>
		 			<div class="row">
			 			<div>
			 				Access to NASP Members Only Portal
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
		 			</div>
		 			<div class="row">
			 			<div>
			 				Access to <em>Journal of Drug Assessment</em> content and Article Processing Charge (APC) discounts
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
		 			</div>
		 			<div class="row">
			 			<div>
			 				Patient resources—for staff training and patient education*
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
		 			</div>
		 			<div class="row">
			 			<div>
			 				Job Board
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
		 			</div>
		 			<div class="row">
			 			<div>
			 				Limited Distribution Drug List*
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
		 			</div>
		 			<div class="row">
			 			<div>
			 				NASP Marketplace—vendor discounts and offers*
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
		 			</div>
		 			<div class="row section_child_row">
			 			<div>
			 				ACHC discounts on accreditation & resources
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
		 			</div>
		 			<?php /* <div class="row section_child_row">
			 			<div>
			 				Dr. First 10% discount
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
		 			</div> */ ?>
				 	
		 			<div class="row price_row">
			 			<div>
				 			Annual dues
			 			</div>
			 			<div>
				 			<?php pricing('palladium_a') ?>
			 			</div>
			 			<div>
				 			<?php pricing('platinum_a') ?>
			 			</div>
			 			<div>
				 			<?php pricing('gold_a') ?>
			 			</div>
			 			<div>
				 			<?php pricing('silver_a') ?>
			 			</div>
			 			<div>
				 			<?php pricing('bronze_a') ?>
			 			</div>
			 			<div>
				 			<?php pricing('nickel_a') ?>
			 			</div>
			 			<div>
				 			<?php pricing('copper_a') ?>
			 			</div>
		 			</div>
		 			<div class="row button_row">
			 			
		 				<?php if ($_GET['package'] == '1'): ?>
		 			
				 			<div>
					 			
				 			</div>
				 			<div>
					 			<a <?php register_url('palladium_a') ?> target="_blank">Choose Palladium</a>
				 			</div>
				 			<div>
					 			<a <?php register_url('platinum_a') ?> target="_blank">Choose Platinum</a>
				 			</div>
				 			<div>
					 			<a <?php register_url('gold_a') ?> target="_blank">Choose Gold</a>
				 			</div>
				 			<div>
					 			<a <?php register_url('silver_a') ?> target="_blank">Choose Silver</a>
				 			</div>
				 			<div>
					 			<a <?php register_url('bronze_a') ?> target="_blank" rel="nofollow">Choose Bronze</a>
				 			</div>
				 			<div>
					 			<a <?php register_url('nickel_a') ?> target="_blank" rel="nofollow">Choose Nickel</a>
				 			</div>
				 			<div>
					 			<a <?php register_url('copper_a') ?> target="_blank" rel="nofollow">Choose Copper</a>
				 			</div>
			 			
			 			<?php else: ?>
			 			
				 			<div>
					 			
				 			</div>
				 			<div>
					 			<a <?php register_url('palladium_b') ?> target="_blank" rel="nofollow">Choose Palladium</a>
				 			</div>
				 			<div>
					 			<a <?php register_url('platinum_b') ?> target="_blank" rel="nofollow">Choose Platinum</a>
				 			</div>
				 			<div>
					 			<a <?php register_url('gold_b') ?> target="_blank" rel="nofollow">Choose Gold</a>
				 			</div>
				 			<div>
					 			<a <?php register_url('silver_b') ?> target="_blank" rel="nofollow">Choose Silver</a>
				 			</div>
				 			<div>
					 			<a <?php register_url('bronze_b') ?> target="_blank" rel="nofollow">Choose Bronze</a>
				 			</div>
				 			<div>
					 			<a <?php register_url('nickel_b') ?> target="_blank" rel="nofollow">Choose Nickel</a>
				 			</div>
				 			<div>
					 			<a <?php register_url('copper_b') ?> target="_blank" rel="nofollow">Choose Copper</a>
				 			</div>
				 			
			 			<?php endif; ?>
			 			
		 			</div>

	 			</div>
			</div>
 		</div>
 		<div class="content_inner">
 		
	 		<h2>
		 		Pharmacy Technician Packages
	 		</h2>
	 		<p>
		 		Now offering Pharmacy Technician Packages you can add to your Corporate Membership. In addition to the <a href="<?php echo site_url('membership-plan/pharmacy-technician') ?>">Individual Pharmacy Technician member benefits</a>, Pharmacy Technician benefits include the following:
	 		</p>
 		
 		</div>
 		
 		<div class="plans_table_outer">
			<div class="plans_table">
	 			<div class="plans_table_inner">
		 			<div class="row table_head">
			 			<div>
				 			
			 			</div>
			 			<div>
				 			<span>Technician Package #1</span>
			 			</div>
			 			<div>
				 			<span>Technician Package #2</span>
			 			</div>
			 			<div>
				 			<span>Technician Package #3</span>
			 			</div>
		 			</div>
		 			<div class="row count_row">
			 			<div>
				 			Pharmacy Technician Members
			 			</div>
			 			<div>
				 			5
			 			</div>
			 			<div>
				 			10
			 			</div>
			 			<div>
				 			20
			 			</div>
		 			</div>
		 			<div class="row">
			 			<div>
			 				NASP Committee Participation
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
		 			</div>
		 			<div class="row">
			 			<div>
			 				NASP Pharmacy Technician Award Eligibility
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
			 			<div class="eligible">
				 			
			 			</div>
		 			</div>
		 			<div class="row">
			 			<div>
			 				Complimentary Annual Meeting & Expo registration
			 			</div>
			 			<div>
				 			
			 			</div>
			 			<div>
				 			1
			 			</div>
			 			<div>
				 			1
			 			</div>
		 			</div>
		 			<div class="row">
			 			<div>
			 				Complimentary Annual Meeting & Expo Workshop
			 			</div>
			 			<div>
				 			
			 			</div>
			 			<div>
				 			
			 			</div>
			 			<div>
				 			1
			 			</div>
		 			</div>
		 			<div class="row price_row">
			 			<div>
				 			Annual dues
			 			</div>
			 			<div>
				 			$500
			 			</div>
			 			<div>
				 			$1,000
			 			</div>
			 			<div>
				 			$2,000
			 			</div>
		 			</div>

	 			</div>
			</div>
 		</div>
	 	
 		<p style="color: #999; margin-top: 20px; font-style: italic;">
	 		* 2021 member benefits coming soon<br>
	 		** Corporate members who upgrade from Nickel to Bronze OR Copper to Nickel may purchase State level legislative monitoring for $2,000 per year
 		</p>
 		
 <?php endif; ?>
		
		<div class="need_help_block">
			<span class="heading">Need help deciding?</span><a class="phone" href="tel:703.842.0122">703.842.0122</a>
		</div>
	    
	</main><?php

get_template_part('template-parts/funnels');

get_footer();

<?php

get_header();

	?><main id="content"> <!-- for ADA compliance -->
	
<?php
		if (has_post_thumbnail()):
			?><div class="banner preload" style="background-image: url('<?php echo get_the_post_thumbnail_url($post, 'full') ?>');">
				<div class="text<?php if (!empty (get_the_field('headline'))) echo ' has_headline'; ?>">
					<h1><?php the_title() ?></h1>
					<div class="headline"><?php the_field('headline') ?></div>
				</div>
			</div>
			<div class="content_inner"><?php
		else:
			?><div class="content_inner">
				<h1><?php the_title() ?></h1><?php
			
			if (!empty(get_the_field('nasp_title'))):
				?><div class="nasp_title"><?php the_field('nasp_title') ?></div><?php
			endif;
		endif;

		if (have_posts()):
			while (have_posts()):
				the_post();
				the_content();
				
				?><div class="contact_info"><?php
					
				if (!empty(get_the_field('phone'))):
				
					?><p>
						Phone: <a href="tel:<?php the_field('phone') ?>"><?php the_field('phone') ?></a>
					</p><?php
				
				endif;
				if (!empty(get_the_field('fax'))):
				
					?><p>
						Fax: <?php the_field('fax') ?>
					</p><?php
				
				endif;
				if (!empty(get_the_field('email'))):
				
					?><p>
						<a href="mailto:<?php the_field('email') ?>"><?php the_field('email') ?></a>
					</p><?php
				
				endif;
				
				?></div><?php
				
			endwhile;
		endif;

	    ?></div>
	</main><?php

get_footer();

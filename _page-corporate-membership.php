<?php

get_header();

	?><main id="content"> <!-- for ADA compliance -->
	
<?php
		if (has_post_thumbnail()):
			?><div class="banner preload" style="background-image: url('<?php echo get_the_post_thumbnail_url($post, 'full') ?>');">
				<div class="text<?php if (!empty (get_the_field('headline'))) echo ' has_headline'; ?>">
					<h1><?php the_title() ?></h1>
					<div class="headline"><?php the_field('headline') ?></div>
				</div>
				<div class="corner"></div>
			</div>
			<div class="content_inner"><?php
		else:
			?><div class="content_inner">
				<h1><?php the_title() ?></h1><?php
		endif;

		if (have_posts()):
			while (have_posts()):
				the_post();
				the_content();
			endwhile;
		endif;
 ?>
		</div>
		<section class="bottom_action">
			<div class="bottom_action_inner">
				
				<h3>
					Compare plans and pricing.
				</h3>
				<p>
					Choose the type of plans you&rsquo;re interested in to see the full list of benefits and compare pricing across membership levels.
				</p>
				<div class="select_plan_buttons">
					<a class="select_plan_button" <?php url('corporate-membership/plans-pricing?specialty-pharmacy') ?>>
						For Specialty Pharmacies
					</a><a class="select_plan_button" <?php url('corporate-membership/plans-pricing?not-specialty-pharmacy') ?>>
						For Other
					</a>
				</div>
			</div>

<!--
			<form>
		    	<fieldset>
			    	<label for="profession">Specialty Pharmacy Status</label>
			    	<select name="profession" id="profession">
				    	<option></option>
						<option value="yes">
							I am a specialty pharmacy.
						</option>
						<option value="yes">
							I am not.
						</option>
				    </select>
		    	</fieldset>
			</form>
-->
		    	
<!--
			<p>
				Choose your membership level to see pricing, additional benefits, and to register.
			</p>
			<div class="wp-block-button">
				<a class="wp-block-button__link" <?php url('corporate-membership-pricing-plans') ?>>
					Compare Membership Pricing & Plans
				</a>
			</div>
-->
		</section>
	    
	</main><?php

get_template_part('template-parts/funnels');

get_footer();

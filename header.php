<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0" />
	<meta name="theme-color" content="#002d74">
	<link rel="shortcut icon" href="<?php echo site_url('wp-content/themes/nasp/assets/images/favicon.png') ?>" type="image/png">
	
	<!-- Typekit (Adobe Fonts) -->
	<script>
	  (function(d) {
	    var config = {
	      kitId: 'xqd1riq',
	      scriptTimeout: 3000,
	      async: true
	    },
	    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
	  })(document);
	</script>
	
	<?php wp_head(); ?>
	
	<style>
		@media ( max-width: 1480px ) and ( min-width: 1001px) {
			header + nav .nav_inner > li > a,
			header + nav .nav_inner > li > span {
				letter-spacing: 0;
				text-transform: none;
				padding-left: 15px;
				padding-right: 15px;
			}
		}
	</style>
	
</head>

<body <?php body_class(); ?>>

	<a class="skip_link screen_reader_text" href="#content">Skip to content</a> <!-- for ADA compliance -->

	<input name="is_top_toggle" type="checkbox" aria-hidden="true" class="is_top_toggle" checked />

	<header>
<?php
	if (!is_front_page()):
		?><a <?php url('') ?> class="logo"><?php include('assets/images/logo_main.svg'); ?></a><?php
	else:
		?><div class="logo"><?php include('assets/images/logo_main.svg'); ?></div><?php
	endif;
?>
	</header>

<?php if (has_nav_menu('primary')): ?>
	<nav aria-label="Primary Menu">
		
		<?php gdusa_build_nav_html('primary') ?>
		
		<div class="sign_in_links">
			<?php render_mini_top_nav(); ?>
			<a class="sign_in_button" href="https://nasp.wildapricot.org/" target="_blank" rel="nofollow">Sign In</a>
			<a class="join_button" <?php url('membership') ?>>Join Now</a>
		</div>
		
	</nav>
<?php endif; ?>

	<div for="nav_toggle" class="nav_hamburger" title="Nav Toggle" aria-hidden="true">
		<span class="a"></span>
	</div>

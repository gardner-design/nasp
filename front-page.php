<?php

get_header();

	?><main id="content"> <!-- for ADA compliance -->
	    <div class="block block_a">
			
			<div class="image preload" <?php bg_image(get_url('wp-content/uploads/2020/11/Washington-DC-Capitol-Image-Annual-Meeting-and-NASP-Home-Page-1536x1020.jpg')) ?>>
				<div class="image_inner"></div>
			</div>
			
		    <h1 class="enter_stage off_stage">
			    The Unified Voice
			    of Specialty Pharmacy
			</h1>
		    <div class="paragraphs">
		    	<p class="enter_stage off_stage">
			    	<em>What’s Special about Specialty Pharmacy?</em> Specialty Pharmacy’s patient-centric design provides a comprehensive and coordinated model of care for patients. The medications and expert services that specialty pharmacies provide expedite patient access to care ensure appropriate medication use and achieve superior clinical and cost-effective outcomes.
		    	</p>
		    	<p class="enter_stage off_stage">
					Through networking opportunities, continuing professional education, certification, and public policy advocacy, NASP provides the specialty pharmacy industry with the support it deserves. Empowered pharmacies, healthier patients, and a better world.
		    	</p>
		    	<p class="enter_stage off_stage">
			    	Together we are stronger, together, we are NASP.
		    	</p>
				<p class="enter_stage off_stage">
					<a <?php url('about') ?>>About NASP</a>
				</p>
		    </div>
			
	    </div>
	    <div class="block block_b">
			
			<div class="image preload parallax" <?php bg_image(get_url('wp-content/uploads/2020/11/A-place-to-Engage-1024x682.jpg')) ?>></div>
		    
		    <div class="text">
			    <h2 class="enter_stage off_stage">2021 NASP Annual Meeting & Expo</h2>
			    <h3 class="enter_stage off_stage">A place to engage.</h1>
		    	<div class="event_description enter_stage off_stage">
			    	<p>
				    	We bring together the industry’s best and brightest in our nation’s capital—where the change makers are and where change happens—to learn about the latest advancements, share best practices, explore potential partnership and synergies, and foster connectivity. Together we are uniquely positioned to change and shape not only the future of specialty pharmacy, but also the patient journey.
			    	</p><p class="date_outer">
						<span class="month">Sep</span><br>
						<span class="dates">27–30</span>
					</p><p class="link_outer enter_stage off_stage">
						<a <?php url('annual-meeting') ?>>Event Info</a>
					</p>
		    	</div>
			</div>
			
	    </div>
	    <div class="block block_c">
			
			<div class="image preload" <?php bg_image(get_url('wp-content/uploads/2020/11/A-place-to-Grow-1024x682.jpg')) ?>></div>
		    
		    <div class="text">
			    <h2 class="enter_stage off_stage">NASP University</h2>
			    <h3 class="enter_stage off_stage">A place to grow.</h1>
		    	<p class="enter_stage off_stage">
			    	<em>We’re deeply committed</em> to advancing specialty pharmacy practice by promoting education and developing programs and services designed to advance and enhance patient care. NASP proudly offers the Certified Specialty Pharmacy (CSP) credential through the Specialty Pharmacy Certification Board (SPCB). Members only online continuing education programs offer unique opportunities for industry stakeholders to expand and enhance their knowledge of our rapidly evolving and dynamic industry.
		    	</p>
				<p style="text-align: right;" class="enter_stage off_stage">
					<a <?php url('education') ?>>About NASP University</a>
				</p>
			</div>
			
	    </div>
	    <div class="join_block">
		    <p>Help shape the future of specialty pharmacy.</p>
		    <a <?php url('membership') ?>>Join Today</a>
	    </div>
	</main><?php

get_footer();

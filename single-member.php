<?php

get_header();

	?><main id="content"> <!-- for ADA compliance -->
	
		<div class="content_inner">
<?php
			if (has_post_thumbnail()):
				echo '<div class="member_image_outer ' . get_the_terms($post, 'team')[0]->slug . '">';
				the_post_thumbnail();
				echo '</div>';
			endif;
?>
			<div class="breadcrumb">
				<a <?php url('leadership') ?>><?php print_r(get_the_terms($post, 'team')[0]->name) ?></a>
			</div>
			<h1><?php the_title() ?></h1><?php
			
			if (!empty(get_the_field('nasp_title'))):
				?><div class="nasp_title"><?php the_field('nasp_title') ?></div><?php
			endif;
	
			if (have_posts()):
				while (have_posts()):
					the_post();
					the_content();
					
					?><blockquote class="wp-block-quote contact_info"><?php
						
					if (!empty(get_the_field('phone'))):
					
						?><p>
							<a href="tel:<?php the_field('phone') ?>"><?php the_field('phone') ?></a>
						</p><?php
					
					endif;
					if (!empty(get_the_field('fax'))):
					
						?><p>
							<?php the_field('fax') ?> (Fax)
						</p><?php
					
					endif;
					if (!empty(get_the_field('email'))):
					
						?><p>
							<a href="mailto:<?php the_field('email') ?>"><?php the_field('email') ?></a>
						</p><?php
					
					endif;
					if (!empty(get_the_field('website_url'))):
					
						?><p>
							<a href="<?php the_field('website_url') ?>" target="_blank" rel="nofollow"><?php echo str_replace(['http://','https://','www.'], '', preg_replace('/\/$/', '', get_the_field('website_url'))) ?></a>
						</p><?php
					
					endif;
					
					?></div><?php
					
				endwhile;
			endif;

	    ?></div>
	    
	    <section class="bottom_action" style="text-align: center">
		    <p>
			    Meet the rest of the NASP board <span>of directors and staff.</span>
		    </p>
		    <div class="wp-block-button aligncenter">
			    <a class="wp-block-button__link" <?php url('leadership') ?>>
				    NASP Leadership
				</a>
			</div>
	    </section>
	    
	</main><?php

get_footer();

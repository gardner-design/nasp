<?php

function add_body_class($classes) {
    $classes[] = 'page';
    return $classes;
}
add_filter('body_class', 'add_body_class');

get_header();

?>	
	<main id="content"> <!-- for ADA compliance -->
	    <div class="content_inner">
	    	<h1>Nothing here.</h1>
	    	<p>
		    	It looks like that page is gone, but you can refer to the menu navigation above to find what you&rsquo;re looking for. 
	    	</p>
	    	<p>
	    		<a <?php url('') ?>>Go to home page</a>
	    	</p>
	    </div>
	</main><?php

get_footer();
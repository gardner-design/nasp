<?php

get_header();

	?><main id="content"> <!-- for ADA compliance -->
        <?php
/*
		if (!empty(single_cat_title('', false))):
			
			?><a class="all_news" <?php url('news') ?>>News</a>
			<h1><?php single_cat_title() ?></h1><?php
				
		else:
			?><h1>News</h1><?php
		endif;
*/
		?>
		
		<div class="news_categories">
			<h1>News</h1>
			<?php /* <h2>Categories</h2> */ ?>
			<div class="categories_outer">
				<a <?php url('news') ?>>
					All
				</a><?php
	
				foreach (get_categories(array( 'number' => 8, 'parent' => 0 )) as $cat):
					if (empty($cat->parent)):
		
					?><a <?php url(get_category_link($cat)) ?>>
						<?php echo $cat->name ?></a><?php
		
					endif;
				endforeach;
					
				?><a <?php url('community-news') ?>>
					Current Community News
				</a>
			</div>
		</div>
		<div class="content_inner">
		    <div class="articles">
				<ul><?php
				
				include_once(ABSPATH.WPINC . '/class-simplepie.php');
				$rss = fetch_feed('https://app.meltwater.com/gyda/outputs/5fe051487b456000b676f0f4/rendering?apiKey=5ad0c644a815866ef36e5442&type=rss', 3);
				$rss_items = $rss->get_items(0, $rss->get_item_quantity( 5 ));
				
				foreach ( $rss_items as $item ): 
				
		            ?><li>
		                <a href="<?php echo esc_url( $item->get_permalink() ); ?>" target="_blank" rel="nofollow noopener noreferrer">
		                    <span class="title"><?php echo esc_html( $item->get_title() ); ?></span>
							<span class="date">
								<?php if ($item->get_date('Y') == date('Y')): echo $item->get_date('M d'); else: echo $item->get_date('M d, Y'); endif; ?>
							</span>
							<span class="excerpt">
								...<?php echo $item->get_content() ?>... <span class="fake_link">read more</span>
							</span>
		                </a>
		            </li><?php 
			            
		        endforeach;
		        
				?></ul>
			</div>
	    
	    </div>
	    <div class="pagination">
		</div>
	</main><?php

get_footer();

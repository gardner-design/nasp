<?php

get_header();

	?><main id="content"> <!-- for ADA compliance -->
	
<?php
		if (has_post_thumbnail()):
			?><div class="banner preload" style="background-image: url('<?php echo get_the_post_thumbnail_url($post, 'full') ?>');">
				<div class="text<?php if (!empty (get_the_field('headline'))) echo ' has_headline'; ?>">
					<h1><?php the_title() ?></h1>
					<div class="headline"><?php echo save_orphans(get_the_field('headline')) ?></div>
				</div>
				<div class="corner"></div>
			</div>
			<div class="content_inner"><?php
		else:
			?><div class="content_inner">
				<h1><?php the_title() ?></h1><?php
		endif;

/*
		if (have_posts()):
			while (have_posts()):
				the_post();
				the_content();
			endwhile;
		endif;
*/
?>
		<div class="corp_members">
<?php
		$terms = get_terms(array(
			'taxonomy' => 'level',
			'hide_empty' => true
		));
		foreach ($terms as $term):
		
			?><h2><?php echo $term->name ?> Members</h2>
			<ul><?php
			
			$args = [
				'post_type' => 'corp-member',
				'nopaging' => 1,
				'orderby' => 'post_title',
				'order' => 'ASC',
				'tax_query' => array(
					array (
						'taxonomy' => 'level',
						'field' => 'term_id',
						'terms' => $term->term_id
					)
				)
			];
			$corp_members = get_posts($args);
			
			if (!empty($corp_members)):
				foreach ($corp_members as $corp_member):
				
					$image_url = get_the_post_thumbnail_url($corp_member, 'large');
					$is_founding_member = (strpos($corp_member->post_title, '*') !== false);

					?><li>
						<?php echo $corp_member->level ?>
						<a href="<?php echo $corp_member->website_url ?>" target="_blank" rel="nofollow">
<?php
							if (!empty($image_url)):
								?><img class="image" src="<?php echo $image_url ?>" alt="<?php echo $corp_member->post_title ?>" /><?php
							else:
								?><span class="name"><?php echo str_replace('*', '', $corp_member->post_title) ?></span><?php
							endif;
							if ($is_founding_member) {
								echo '<span class="tag">Founding member</span>';
							}
?>
						</a>
					</li><?php
				
				endforeach;
			endif;
			
			?></ul><?php
			
		endforeach;
?>
			</div>
	</main><?php
		
get_template_part('template-parts/funnels');

get_footer();

<?php

get_header();

	?><main id="content"> <!-- for ADA compliance -->
		<div class="content_inner">
			<h1>
				<?php the_title() ?>
			</h1>
			<form class="search_form">
				<fieldset class="search_outer notempty">
					<label for="location">Location</label>
					<select id="location" name="l">
						<option value="all">All Locations</option>
<?php
						foreach (get_job_locations() as $key => $value):
						
							?><option value="<?php echo $key ?>"><?php echo $value ?></option><?php
								
						endforeach;
?>
					</select>
				</fieldset>
			</form>
		    <div class="jobs">
<?php
			if (!empty(get_jobs())):
				
				foreach (get_jobs() as $job):
				
					$location_id =	str_replace(array(','), '',
										str_replace(' ', '-',
											strtolower(get_post_meta($job->ID,'location',true))));
		
					?><a class="job job_<?php the_ID() ?> location-<?php echo $location_id ?>" href="<?php echo get_permalink($job) ?>">
						<span class="title">
							<?php echo $job->post_title; ?>
						</span>
	<?php
						if (!empty(get_post_meta($job->ID, 'company', true))):
						
							?><span class="company"><?php echo get_post_meta($job->ID, 'company', true) ?></span><?php
						
						endif;
						if (!empty(get_post_meta($job->ID, 'location', true))):
						
							?><span class="location"><?php echo get_post_meta($job->ID, 'location', true) ?></span><?php
						
						endif;
	?>
						<?php /* <span class="excerpt">
							<?php echo get_the_excerpt($job) ?>
						</span> */ ?>
						<span class="fake_link">
							Learn More
						</span>
					</a><?php
		
				endforeach;
			else:
				echo '<p class="no_jobs_message">No jobs currently listed.</p>';
			endif;
?>
			</div>
<?php
			if (have_posts()):
				while (have_posts()):
					the_post();
					the_content();
				endwhile;
			endif;
?>
	    </div>
	</main><?php

get_footer();

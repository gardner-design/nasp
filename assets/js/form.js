var textInputs = 'textarea, input[type=color], input[type=date], input[type=datetime], input[type=datetime-local], input[type=email], input[type=month], input[type=number], input[type=password], input[type=search], input[type=tel], input[type=text], input[type=time], input[type=url], input[type=week]';

$(function() {
	
	$(textInputs + ', select').on('keydown change focus blur', function(e) {
		var val = $(this).val();
		
		if (isPrintable(e)) {
			val += e.key;
		}
		if (e.key == 'Backspace') {
			val = val.slice(0, -1);
		}
		$(this).parents('fieldset').toggleClass('notempty', (val != ''));
	});
	
	checkEmpty();
	
});

function isPrintable(e) {
    var keycode = e.keyCode;
    var valid = 
        (keycode > 47 && keycode < 58) ||   // number keys
        keycode == 32 || keycode == 13 ||   // spacebar & return key(s) (if you want to allow carriage returns)
        (keycode > 64 && keycode < 91) ||   // letter keys
        (keycode > 95 && keycode < 112) ||  // numpad keys
        (keycode > 185 && keycode < 193) || // ;=,-./` (in order)
        (keycode > 218 && keycode < 223);   // [\]' (in order)
    return valid;
}
function checkEmpty() {
	$(textInputs + ', select').each(function() {
		$(this).parents('fieldset').toggleClass('notempty', ($(this).val() != ''));
	});
}
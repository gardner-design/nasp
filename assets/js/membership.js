var paged = 0;

$(function() {
	
	// initMembership();
	
	$('#corp_type').change(function() {
		var val = $(this).val();
		if (val == '') return;
		parent.location = 'membership/corporate-plans-pricing?package=' + val;
	});
	$('#indiv_type').change(function() {
		var val = $(this).val();
		if (val == '') return;
		parent.location = 'membership-plan/' + val;
	});
	
});

/*
function initMembership() {
	$('#profession').change(function() {
		var val = $(this).val();
		if (val == '') return;
		$('#profession_form').hide();
		$('.benefits').hide();
		$('.benefits.' + val).show();
	});
	$('.back_to_profession').click(function() {
		var val = $(this).val();
		$('.benefits').hide();
		$('#profession_form').show();
	});
}

function initMembership() {
	
	$('.select_plan_button').click(function() {
		$('.popup').addClass('visible');
		$('body').addClass('freeze');
	});
	$('.cancel_button, .pages_background').click(function() {
		$('.popup').removeClass('visible');
		$('body').removeClass('freeze');
	});

	$('.page_prev_button').click(function() {
		navPages(-1);
	});
	$('.page_next_button').click(function() {
		navPages(1);
	});
	
	$('[name="profession"]').change(function() {
		var val = $(this).val();
		$('.page_next_button').toggleClass('enabled', (val != ''));
		$('.page_4').toggle((val == 'pharmacist'));
	});
	$('[name="corporate-plan"]').change(function() {
		var val = $(this).val();
		$('.page_next_button').toggleClass('enabled', (val != ''));
		
		$('.package_benefits').hide();
		$('.package_benefits.' + val).show();
	});
}
function navPages(i) {
	paged += i;
	if (paged < 0) paged = 0;
	if (paged > $('.pages .page:visible').length - 1) paged = $('.pages .page:visible').length - 1;
	var pages = $('.pages');
	pages[0].className = 'pages';
	pages.addClass('paged_' + paged);
	
	$('.dots').attr('data-count', $('.pages .page:visible').length);
	
	if (paged == $('.pages .page:visible').length - 1) {
		
		$('.page_next_button').hide();
		
		var key = $('[name=membership_type]:checked').val();
		if (key == 'individual') {
			key += '-' + $('[name=profession]').val();
			
			if ($('[name=profession]').val() == 'pharmacist' && $('[name=is-certified-pharm]:checked').val() == 'yes') {
				key += '-csp';
			}
		} else if (key == 'corporate') {
			key += '-' + $('[name=corporate-plan]').val() + '-' + $('[name=benefits-package]:checked').val();
			
		}
		console.log(key);
		
		$('.benefits').hide();
		$('.benefits.' + key).show();
	} else {
		$('.page_next_button').show();
	}
}
*/
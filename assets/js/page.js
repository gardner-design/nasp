var scrolling = null,
	wasTop = true,
	parallaxEls = [];

$(function($) {

	// initParallax();

	initPreload();

	initAlphabeticalLists();

	// Scrolling down page changes header style
	toggleHeaderStyle();
	$(document).scroll(function() {
		toggleHeaderStyle();
	});

	$('.nav_hamburger').click(function() {
		$('header + nav').toggleClass('open');
	});

	$('.item.has_menu > span').click(function() {
		var el = $(this).parents('.item');
		if (el.hasClass('open')) {
			el.removeClass('open');
		} else {
			$('.item.has_menu').removeClass('open');
			el.addClass('open');
		}
	});

	$(function miniNavToggle() {
		let $body = $('body');

		if( $body.width() < 1000 ) {
			// grab the height of each sub-menu for the slide function, then collapse it
			$('.sub-menu').each(function() {
				$height = $(this).height();
				$(this).css('height', $height);
				$(this).hide();
			});

			$('.menu-item-has-children > a').click(function(e) {
				$(this).toggleClass('chevron-rotate');
				var $sub = $(this).next('.sub-menu:first');
				$sub.slideToggle(300);
				e.preventDefault(); // prevent bubbling
			});
		}
	});

	// Scrolling down page reveals items on page
	enterStage();
	$(document).scroll(function() {

		if (scrolling == null) {
			scrolling = setTimeout(function() {
				scrolling = null;
				enterStage();
			}, 500);
		}
	});

	initJobSearch();

});

function toggleHeaderStyle() {
	var isTop = jQuery(document).scrollTop() <= 66;

	if (isTop != wasTop) {
		wasTop = isTop;
		$('.is_top_toggle').prop('checked', isTop);
	}
}

// Preload images before fading in
function initPreload() {
	$('.preload').each(function() {
		var real = $(this);
		var fake = new Image();
		if ($(this).is('img')) {
			fake.src = $(this).attr('src');
		} else {
			fake.src = /url\((.*)\)/.exec($(this).css('background-image'))[1].replace(/["']/g,'');
		}
		fake.onload = function() {
			real.removeClass('preload');
		};
	});
}

/*
function initParallax() {
	
	$(document).scroll(function() {
		updateParallaxY();
	});
	
	$('.parallax').each(function() {
		
		var el = $(this).clone();
		$(this).css({ opacity: '0' });
		el.removeClass('parallax');
		el.addClass('parallax_clone');
		el.attr('data-offset-top', $(this).offset().top);
		var w = $(this).width();
		var h = $(this).height();
		el.css({ width: w, height: h, marginTop: -h/2 });
		
		el.appendTo($('body'));
		parallaxEls.push(el);
		
		updateParallaxY();
		
	});
	
}
function updateParallaxY() {
	for (i = 0; i < parallaxEls.length; i++) {
		var el = parallaxEls[i];
		
		var offsetTop = parseFloat(el.attr('data-offset-top'));
		// var t = parseFloat(el.attr('data-offset-top')) + (el.height() / 2) - ($(document).scrollTop() * 1.25);
		
		var t = offsetTop - ($(window).height() / 2) + (el.height() / 2) - ($(document).scrollTop());
		
		var x = $(window).scrollTop() / (offsetTop + (el.height() / 2) - ($(window).height() / 2)) - 1;
		
		t -= 100 * x;
		
		el.css({ transform: 'translateY(' + t + 'px)' });
		
	}
}
*/

function enterStage() {
	var scrollFromTop = $(document).scrollTop();
	var i = 0;
	
	$('.off_stage').each(function() {
		var item = $(this);
		var thisFromTop = item.offset().top,
			thisHeight = item.height();

		var isInFrame = (scrollFromTop > thisFromTop - $(window).height()); // && thisFromTop < scrollFromTop + $(window).height());
		
		if (thisFromTop < scrollFromTop) { // if above scroll point
			i = 0;
		}
		
		if (isInFrame) {
			setTimeout(function() {
				item.removeClass('off_stage');
				
			}, 200*i);
			i++;
		}
		
	});
}

function initAlphabeticalLists() {
	var list, i, switching, b, shouldSwitch;
	$('ul.alphabetical').each(function() {
		var list = $(this)[0];
		
		switching = true;
		while (switching) {
			switching = false;
			b = list.getElementsByTagName("li");
			for (i = 0; i < (b.length - 1); i++) {
				shouldSwitch = false;
				if (b[i].innerText.toLowerCase() > b[i + 1].innerText.toLowerCase()) {
					shouldSwitch = true;
					break;
				}
			}
			if (shouldSwitch) {
				b[i].parentNode.insertBefore(b[i + 1], b[i]);
				switching = true;
			}
		}
		
	});
}

function initJobSearch() {
	$('#location').change(function() {
		var locationId = $(this).val();
		if (locationId != 'all') {
			$('.job').addClass('inactive');
			$('.job.location-' + locationId).removeClass('inactive');
		} else {
			$('.job').removeClass('inactive');
		}
	});
}

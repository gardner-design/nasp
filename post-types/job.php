<?php

gardner_cpt((object) [
	'id' =>           'job',
	'label' =>        'Job',
	'label_plural' => 'Jobs',
	'icon' => 		  'pressthis',
	'show_in_rest' => true,
	'supports' =>     array('title','editor','excerpt'),
	'has_archive' => false,
    'capability_type' => array('job','jobs'),
    'map_meta_cap' => true,
	'boxes' =>        array(
		'details' => (object) [
			'context' => 'side',
			'label' => 'Details',
			'fields' => array(
				'company' => (object) [
					'label' => 'Company',
					'type' => 'text'
				],
				'location' => (object) [
					'label' => 'Location',
					'type' => 'text'
				],
				'apply_link' => (object) [
					'label' => 'Apply Link',
					'type' => 'text'
				]
			)
		],
		'expiry' => (object) [
			'context' => 'side',
			'label' => 'Expiration',
			'fields' => array(
				'expiration_date' => (object) [
					'label' => 'Jobs expire 30 days after posting, unless an expiration date is included here.',
					'type' => 'date'
				]
			)
		]
	)
]);

// Add custom column to admin list page
function set_job_columns($columns) {
	$columns = [
	    'cb' => '<input type="checkbox" />',
	    'title' => 'Job Title',
	    'date' => 'Date'
    ];
    return $columns;
}
add_filter( 'manage_job_posts_columns', 'set_job_columns' );

function handle_job_column( $column, $post_id ) {
    switch ( $column ) {
        case 'job_title':
        	echo get_post_meta($post_id, 'job_title', true);
            break;
    }
}
add_action( 'manage_job_posts_custom_column' , 'handle_job_column', 10, 2 );

// User role for custom post type

add_role('gdusa_jobs',
    'Jobs Manager',
    array(
        'read' => true,
        'edit_posts' => false,
        'delete_posts' => false,
        'publish_posts' => false,
        'upload_files' => true,
		'read_job' => true,
		'read_private_job'  => true,
		'edit_job'  => true,
		'edit_jobs'  => true,
		'edit_others_jobs'  => true,
		'edit_published_jobs'  => true,
		'publish_jobs'  => true,
		'delete_others_jobs'  => true,
		'delete_private_jobs'  => true,
		'delete_published_jobs'  => true
    )
);

function get_jobs() {
	
	$args = [
		'post_type' => 'job',
		'nopaging' => 1,
		'meta_query' => array(
			'relation' => 'or',
			array(
				'key' => 'expiration_date',
				'value' => date('Y-m-d'),
				'compare' => '>='
			),
			array(
				'key' => 'expiration_date',
				'value' =>  '',
				'compare' => '='
			)
		)
	];
	$jobs = get_posts($args);
	$not_expired_jobs = [];
	foreach ($jobs as $job) {
		
		// Jobs expire after 30 days
		
		if (date('Y-m-d', strtotime("-30 days")) <= get_the_date('Y-m-d', $job) || !empty(get_post_meta($job->ID, 'expiration_date', true))) {
			$not_expired_jobs[] = $job;
		}
	}
	
	return $not_expired_jobs;
}

function get_job_locations() {
	$locations = [];
	foreach (get_jobs() as $job) {
		$location_id =	str_replace(array(','), '',
							str_replace(' ', '-',
								strtolower(get_post_meta($job->ID,'location',true))));
		$locations[$location_id] = get_post_meta($job->ID,'location',true);
	}
	return $locations;
}
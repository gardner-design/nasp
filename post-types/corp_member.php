<?php

gardner_cpt((object) [
	'id' =>           'corp-member',
	'label' =>        'Corporate Member',
	'label_plural' => 'Corporate Members',
	'icon' => 		  'businessman',
	'supports' =>     array('title','thumbnail'),
	'boxes' =>        array(
		'details' => (object) [
			'context' => 'normal',
			'label' => 'Details',
			'fields' => array(
				'website_url' => (object) [
					'label' => 'Website URL',
					'type' => 'text'
				]
			)
		]
	)
]);

function corp_member_taxonomy_init() {
	// create a new taxonomy
	register_taxonomy(
		'level',
		'corp-member',
		array(
			'label' => 'Level',
			'labels' => cpt_labels('level', 'Level', 'Levels'),
			'capabilities' => array(),
			'hierarchical' => true,
			'show_in_rest' => true,
			'show_admin_column' => true,
			'show_in_quick_edit' => true
		)
	);
}
add_action( 'init', 'corp_member_taxonomy_init' );

// Add custom column to admin list page
function set_corp_member_columns($columns) {
	
	echo 'jay';
	print_r($columns);
	
	$columns = [
	    'cb' => '<input type="checkbox" />',
	    'title' => 'Name',
	    'image' => 'Image',
		'taxonomy-level' => 'Levels',
	    'date' => 'Date'
    ];
    return $columns;
}
add_filter( 'manage_corp-member_posts_columns', 'set_corp_member_columns' );

function handle_corp_member_column( $column, $post_id ) {
    switch ( $column ) {
        case 'image':
        	the_post_thumbnail('medium', 'style=width: auto !important; height: auto !important; max-width: 200px; max-height: 30px;');
            break;
    }
}
add_action( 'manage_corp-member_posts_custom_column' , 'handle_corp_member_column', 10, 2 );

<?php

gardner_cpt((object) [
	'id' => 		  'link',
	'label' =>        'Link',
	'label_plural' => 'Links',
	'supports' => array('title','thumbnail'),
	'show_in_menu' => false,
	'boxes' => array(
		'funnels' => (object) [
			'label' => 'Bucket Details',
			'context' => 'normal',
			'fields' => array(
				'bucket_text' => (object) [
					'label' => 'Bucket Text',
					'type' => 'text'
				],
				'bucket_url' => (object) [
					'label' => 'URL',
					'type' => 'text'
				]
			)
		]
	)
]);

function add_links_menu_item() { 
	add_submenu_page( 'edit.php?post_type=page', 'Links', 'Links', 'edit_posts', 'edit.php?post_type=link');
}
add_action('admin_menu', 'add_links_menu_item');
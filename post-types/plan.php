<?php

gardner_cpt((object) [
	'id' => 		  'plan',
	'label' =>        'Plan',
	'label_plural' => 'Plans',
	'icon' => 		  'image-filter',
	'supports' => array('title','editor'),
	'show_in_rest' => true,
	'rewrite' => array(
		'slug' => 'membership-plan',
		'with_front' => false
	),
	'boxes' => array(
		'details' => (object) [
			'label' => 'Details',
			'context' => 'side',
			'fields' => array(
				'type' => (object) [
					'label' => 'Type',
					'type' => 'select',
					'options' => array(
						'corporate' => 'Corporate',
						'individual' => 'Individual'
					)
				],
				'price' => (object) [
					'label' => 'Price',
					'type' => 'text'
				],
				'url' => (object) [
					'label' => 'Register URL',
					'type' => 'text'
				]
			)
		],
		'packages' => (object) [
			'label' => 'Packages',
			'context' => 'normal',
			'fields' => array(
				'package_a' => (object) [
					'label' => 'Package 1 (for specialty pharmacies)',
					'type' => 'editor'
				],
				'package_a_url' => (object) [
					'label' => 'Register URL for Package 1',
					'type' => 'text'
				],
				'package_b' => (object) [
					'label' => 'Package 2 (for other corporate members)',
					'type' => 'editor'
				],
				'package_b_url' => (object) [
					'label' => 'Register URL for Package 2',
					'type' => 'text'
				]
			)
		]
	)
]);
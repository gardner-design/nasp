<?php

gardner_cpt((object) [
	'id' =>           'member',
	'label' =>        'Team Member',
	'label_plural' => 'Leadership',
	'icon' => 		  'admin-users',
	'show_in_rest' => true,
	'supports' =>     array('title','editor','thumbnail'),
	'rewrite' => array(
		'slug' => 'leadership',
		'with_front' => false
	),
	'boxes' =>        array(
		'details' => (object) [
			'context' => 'side',
			'label' => 'Details',
			'fields' => array(
				'nasp_title' => (object) [
					'label' => 'NASP Title',
					'type' => 'text'
				],
				'phone' => (object) [
					'label' => 'Phone',
					'type' => 'text'
				],
				'fax' => (object) [
					'label' => 'Fax',
					'type' => 'text'
				],
				'email' => (object) [
					'label' => 'Email',
					'type' => 'text'
				],
				'website_url' => (object) [
					'label' => 'Website',
					'type' => 'text'
				]
			)
		]
	)
]);

function member_taxonomy_init() {
	// create a new taxonomy
	register_taxonomy(
		'team',
		'member',
		array(
			'label' => 'Team',
			'labels' => cpt_labels('team', 'Team', 'Teams'),
			'capabilities' => array(),
			'hierarchical' => false,
			'show_in_rest' => true
			// 'rewrite' => array( 'slug' => 'calendar' )
		)
	);
}
add_action( 'init', 'member_taxonomy_init' );

// Add custom column to admin list page
function set_member_columns($columns) {
	$columns = [
	    'cb' => '<input type="checkbox" />',
	    'title' => 'Name',
	    'team' => 'Team',
	    'date' => 'Date'
    ];
    return $columns;
}
add_filter( 'manage_member_posts_columns', 'set_member_columns' );

function handle_member_column( $column, $post_id ) {
    switch ( $column ) {
        case 'team' :
        	$categories = get_the_terms($post_id, 'team');
        	if (!empty($categories)) {
	            foreach ($categories as $index => $category) {
		            echo $category->name . ($index + 1 == count($categories) ? '' : ', ');
	            }
            }
            break;

    }
}
add_action( 'manage_member_posts_custom_column' , 'handle_member_column', 10, 2 );

<?php

gardner_cpt((object) [
	'id' => 'page',
	'boxes' => array(
		'headline' => (object) [
			'context' => 'side',
			'label' => 'Headings',
			'fields' => array(
				'headline' => (object) [
					'label' => 'Headline',
					'type' => 'textarea'
				],
				'bucket_text' => (object) [
					'label' => 'Bucket Text',
					'type' => 'textarea'
				]
			)
		],
		'funnels' => (object) [
			'context' => 'side',
			'label' => 'Bottom Buckets',
			'fields' => array(
				'funnels' => (object) [
					'type' => 'select',
					'multiple' => true,
					'options_label' => 'pages',
					'options' => get_funnel_options()
				]
			)
		]
	)
]);

function get_funnel_options() {
	$all_page_options = array();
	
	$args = array(
		'post_type' => array('page', 'link'),
		'nopaging' => 1,
		'orderby' => 'title',
		'order' => 'ASC'
	);
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) { $loop->the_post();
		$all_page_options[get_the_ID()] = (!empty(wp_get_post_parent_id(get_the_ID())) ? get_post(wp_get_post_parent_id(get_the_ID()))->post_title . ' &rsaquo; ' : '') . get_the_title();
	}
	wp_reset_postdata();
	
	return $all_page_options;
}
<?php

gardner_cpt((object) [
	'id' =>           'staff_member',
	'label' =>        'Staff Member',
	'label_plural' => 'Staff Members',
	'icon' => 		  'admin-users',
	'show_in_rest' => true,
	'supports' =>     array('title','editor','thumbnail'),
	'rewrite' => array(
		'slug' => 'staff',
		'with_front' => false
	),
	'boxes' =>        array(
		'details' => (object) [
			'context' => 'side',
			'label' => 'Details',
			'fields' => array(
				'nasp_title' => (object) [
					'label' => 'NASP Title',
					'type' => 'text'
				],
				'phone' => (object) [
					'label' => 'Phone',
					'type' => 'text'
				],
				'fax' => (object) [
					'label' => 'Fax',
					'type' => 'text'
				],
				'email' => (object) [
					'label' => 'Email',
					'type' => 'text'
				]
			)
		]
	)
]);

<?php

get_header();

	?><main id="content"> <!-- for ADA compliance -->
	
		<div class="content_inner">
			<h1><?php the_title() ?></h1>
<?php
			if (have_posts()):
				while (have_posts()):
					the_post();
					
					?><p class="details"><?php
						
					if (!empty(get_the_field('company'))):
					
						the_field('company');
					
					endif;
					if (!empty(get_the_field('location'))):
					
						?> &middot; <?php the_field('location');
					
					endif;
					
					?></p><?php
					
					the_content();

					if (!empty(get_the_field('apply_link'))):
					
						?><div class="wp-block-button is-style-squared">
							<a class="wp-block-button__link" href="<?php the_field('apply_link') ?>" target="_blank" rel="noopener noreferrer">
								Apply Now
							</a>
						</div><?php
					
					endif;
					
				endwhile;
			endif;
?>
	    </div>
	</main><?php

get_footer();

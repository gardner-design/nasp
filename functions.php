<?php

date_default_timezone_set("America/Chicago");

define('LAST_COMMIT', '07792021');

// Include custom plugins
require_once('blocks/gdusa_custom.php');

// Include custom post types
require_once('post-types/page.php');
require_once('post-types/member.php');
require_once('post-types/link.php');
require_once('post-types/plan.php');
require_once('post-types/corp_member.php');
require_once('post-types/job.php');

// Add support for responsive embedded content
function gdusa_theme_setup() {
	add_theme_support( 'responsive-embeds' );
	add_theme_support( 'align-wide' );
	register_nav_menu( 'mini-top', 'Mini Top Nav' );
}
add_action( 'after_setup_theme', 'gdusa_theme_setup' );

function render_mini_top_nav() {
	wp_nav_menu([
		'theme_location' => 'mini-top',
		'container'      => false,
		'menu_id'        => 'mini-top-nav',
	]);
}

// Enqueue parent theme styles
function enqueue_parent_styles() {
	
	if (!is_admin()) { // if not is wp-admin
		
		wp_deregister_script('jquery');
		wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', array(), null, true);

		// Add CSS styles
		wp_enqueue_style( 'styles', get_stylesheet_directory_uri() . '/assets/css/styles.css', array(), LAST_COMMIT, 'all' ); // Add styles.css

		// Add JS files
		// wp_enqueue_script( 'jquery-scripts', get_template_directory_uri() . '/assets/third-party/jquery-latest.min.js', array(), '', false );
		// wp_enqueue_script( 'jquery-mobile-scripts', get_template_directory_uri() . '/assets/third-party/jquery.mobile.custom.min.js', array(), '', true );
		wp_enqueue_script( 'base-scripts', get_template_directory_uri() . '/assets/js/page.js', array(), LAST_COMMIT, true );
		wp_enqueue_script( 'form-scripts', get_template_directory_uri() . '/assets/js/form.js', array(), LAST_COMMIT, true );
		wp_enqueue_script( 'membership-scripts', get_template_directory_uri() . '/assets/js/membership.js', array(), LAST_COMMIT, true );
		
	}
		
	add_editor_style( get_template_directory_uri() . '/assets/css/editor-style.css');

}
add_action('wp_enqueue_scripts', 'enqueue_parent_styles');

/*
// Add support for editor styles.
add_theme_support( 'editor-styles' );

// Enqueue editor styles.
add_editor_style( 'assets/css/editor.css' );
*/

/*
// Remove newlines from content
function remove_newlines($content) {
	global $post;
	$content = preg_replace('/\n/','', $content); // remove new lines
	$content = preg_replace('/<div class="content_inner">$/', '', $content);
	
	return $content;
}
add_filter('the_content', 'remove_newlines', 20);

function alter_columns($content) {

	$content = preg_replace('/(<div class="wp-block-columns has-2-columns full">.*<\/div>.?<\/div>(\s\s|$))/sU', '</div>$1<div class="content_inner">', $content);
	$content = preg_replace('/(<div class="content_inner">)$/s', '', $content); // If content ends with this
	
	return $content;
}
add_filter( 'the_content', 'alter_columns' );
*/

// Change default excerpt length
add_filter('excerpt_length', function($length) {
    return 16;
});

/*
// Custom style for login screen
function my_custom_login_stylesheet() {
   wp_enqueue_style( 'styles', get_stylesheet_directory_uri() . '/assets/css/styles_login.css' );
}
add_action( 'login_enqueue_scripts', 'my_custom_login_stylesheet' );
*/

add_post_type_support( 'page', 'excerpt' );

// Get register URL for membership level
function register_url($type) {
	$slug = explode('_', $type)[0];
	$package_type = (count(explode('_', $type)) > 1 ? explode('_', $type)[1] : null);
	
	$meta_key = 'url';
	if (!empty($package_type)) {
		$meta_key = 'package_' . $package_type . '_url';
	}
	
	$plan = get_page_by_path($slug, OBJECT, 'plan');
	$url = get_post_meta($plan->ID, $meta_key, true);
	
	echo 'href="' . $url . '"';
}

// Get pricing for membership level
function pricing($type) {
	$slug = explode('_', $type)[0];
	$package_type = (count(explode('_', $type)) > 1 ? explode('_', $type)[1] : null);
	
	$meta_key = 'price';
	
	$plan = get_page_by_path($slug, OBJECT, 'plan');
	$price = get_post_meta($plan->ID, $meta_key, true);
	
	echo '$' . $price;
}

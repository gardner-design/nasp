<?php

get_header();

	?><main id="content"> <!-- for ADA compliance -->
	
<?php
		if (has_post_thumbnail()):
			?><div class="banner preload" style="background-image: url('<?php echo get_the_post_thumbnail_url($post, 'full') ?>');">
				<div class="text<?php if (!empty (get_the_field('headline'))) echo ' has_headline'; ?>">
					<h1><?php the_title() ?></h1>
					<div class="headline"><?php echo save_orphans(get_the_field('headline')) ?></div>
				</div>
				<div class="corner"></div>
			</div>
			<div class="content_inner"><?php
		else:
			?><div class="content_inner">
				<h1><?php the_title() ?></h1><?php
		endif;

		if (have_posts()):
			while (have_posts()):
				the_post();
				the_content();
			endwhile;
		endif;

		    ?><div class="wp-block-group alignwide benefits_section">
			    <div class="wp-block-group__inner-container">
<?php
	
				$args = [
					'post_type' => 'page',
					'nopaging' => 1,
					'post_parent' => $post->ID
				];
				$committees = new WP_Query($args);
				if ($committees->have_posts()):
					while ($committees->have_posts()):
						$committees->the_post();
			
					?><div class="wp-block-group">
						<div class="wp-block-group__inner-container">
							<div class="image" <?php bg_image('committees/' . $post->post_name . '.png') ?>></div>
							<h4><?php echo str_replace(' Committee', '', get_the_title()) ?></h4>
							
							<?php the_excerpt() ?>
							<p>
								<a href="<?php the_permalink($post) ?>">More about the <?php the_title() ?></a>
							</p>
						</div>
					</div><?php
						
					
					endwhile;
				endif;
				wp_reset_postdata();
?>
				</div>
			</div>
		</div>
	    
	    <?php /* <section class="bottom_action" style="text-align: center">
		    <p>
			    If you would like more information or to join a committee, please contact us.
		    </p>
		    <div class="wp-block-button aligncenter">
			    <a class="wp-block-button__link" <?php url('contact/committees') ?>>
				    Join a Committee
				</a>
			</div>
	    </section> */ ?>
	    
	</main><?php

get_template_part('template-parts/funnels');

get_footer();
